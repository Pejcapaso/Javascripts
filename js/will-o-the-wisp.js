var fireSize = window.innerHeight;
var sizeFrequency = 42;
var sizes = [{size: fireSize / 20, index: 7, brightness: 190},
			 {size: fireSize / 25, index: 6, brightness: 170},
			 {size: fireSize / 30, index: 5, brightness: 140},
			 {size: fireSize / 35, index: 4, brightness: 110},
			 {size: fireSize / 40, index: 3, brightness: 80},
			 {size: fireSize / 45, index: 2, brightness: 70},
			 {size: fireSize / 50, index: 1, brightness: 60}];
var colors = [{colorName: "aqua", colorHex: "#00ffff"}, 
              {colorName: "blue", colorHex: "#0000ff"},
              {colorName: "emerald", colorHex: "#50C878"}, 
              {colorName: "green", colorHex: "#00ff00"}, 
              {colorName: "indigo", colorHex: "#4b0082"},
              {colorName: "lime", colorHex: "#bfff00"},
              {colorName: "magenta", colorHex: "#ff00ff"},  
              {colorName: "orange", colorHex: "#ffa500"},
              {colorName: "red", colorHex: "#ff0000"},
              {colorName: "yellow", colorHex: "#ffff00"}];
var color = getColor(); // get random color
var baseRadius = window.innerHeight / 20;
var swampWidth = window.innerWidth - baseRadius;
var swampHeight = window.innerHeight - baseRadius;
var fireBrightness = 0;

function run(){
    var id = setInterval(start, 1000);
    var count = 0;
    function start() {
    if (count > 50) {
      clearInterval(id);
    } else { 
      count++;
      createFire();
    }
  }
}

function createFire(){  // making a fireball
var fire = document.createElement("div");
fire.classList.add("dot");
fire.classList.add("glow");
var coordinates = getCoordinates();  // get random coordinates
fire.style.top = coordinates[1] + "px";
fire.style.left = coordinates[0] + "px";
fire.style.backgroundColor = colors[color].colorHex;
var currentSize = getSize();  // get random size
fire.style.width = fire.style.height = sizes[currentSize].size + "px";
fire.style.zIndex = sizes[currentSize].index;  // get random priority
fireBrightness = sizes[currentSize].brightness;
var element = document.getElementById("container");
element.appendChild(fire);
var behavior = getBehavior();   // get random behavior
if (behavior == 0){
	move(fire, coordinates[1]);
}
else glow(fire);
}

function getBehavior(){  // stay or move
	var behavior = Math.floor((Math.random() * 2));
    return behavior;
}

function getCoordinates(){  // random coordinates
	var coordX = Math.floor((Math.random() * (swampWidth - baseRadius) + baseRadius));   // appear completely inside the viewport
	var coordY = Math.floor((Math.random() * (swampHeight - baseRadius) + baseRadius)); // appear below the equator
	return [coordX, coordY];
}

function getSize(){  // random size
	var size = Math.floor(Math.random() * 6);
    return size;
}

function getSpeed(){  // random speed
	var speed = Math.floor(Math.random() * 5 + 1);
    return speed;
}

function getColor(){  // picking the same random color for fire and halo
var color = Math.floor(Math.random() * colors.length);
var cssAnimation = document.createElement('style');
cssAnimation.type = 'text/css';
var rules = document.createTextNode('@keyframes glow {'+
'from { box-shadow: 0 0 2vh 0.1vh ' + colors[color].colorHex + ',' + '0 0 2vh 0.1vh ' + colors[color].colorHex + ',' +
'0 0 3vh 0.1vh ' + colors[color].colorHex + ',' + '0 0 4vh 0.1vh ' + colors[color].colorHex + ',' + 
'0 0 5vh 0.1vh ' + colors[color].colorHex + ',' +'0 0 6vh 0.1vh ' + colors[color].colorHex + ',' + '0 0 7vh 0.1vh ' + colors[color].colorHex +'}'+
'to { box-shadow: 0 0 3vh 0.3vh ' + colors[color].colorHex + ',' + '0 0 3vh 0.3vh ' + colors[color].colorHex + ',' +
'0 0 4vh 0.3vh ' + colors[color].colorHex + ',' + '0 0 5vh 0.3vh ' + colors[color].colorHex + ',' + 
'0 0 6vh 0.3vh ' + colors[color].colorHex + ',' +'0 0 7vh 0.3vh ' + colors[color].colorHex + ',' + '0 0 8vh 0.3vh ' + colors[color].colorHex +'}');
cssAnimation.appendChild(rules);
document.getElementsByTagName("head")[0].appendChild(cssAnimation);
return color;
}


function move(fire, y){   // dynamic fire logic
  var id = setInterval(up, 100);
  var brightness = 0;
  fire.style.filter = "brightness(" + brightness + "%)";
  var currentIndex = fire.style.zIndex;
  fire.style.zIndex = -1;
  var speed = getSpeed();
  function up() {
    if (y < -baseRadius * 2) {   //destroying condition
      clearInterval(id);
      removeFire(fire);
      createFire();
    } else { 
      y -= speed;  
      fire.style.top = y + "px"; 
	  if (brightness < fireBrightness){   // slowly light uo
      fire.style.filter = "brightness(" + brightness + "%)";
      brightness += 5;
  	  }
  	  if (brightness >= 50){
  	  	fire.style.zIndex = currentIndex;
  	  }
    }
  }
}

function glow(fire){  // static fire logic
   var id = setInterval(gleam, 200);
   var brightnessWaxing = 0;
   fire.style.filter = "brightness(" + brightnessWaxing + "%)";
   var filter = 300;
   var brightnessWaning = 100;
   var currentIndex = fire.style.zIndex;
   fire.style.zIndex = -1;
  function gleam() {
    if (filter === 0) {
      clearInterval(id);
      removeFire(fire);
      createFire();
    } else { 
      filter--;
      if (brightnessWaxing < fireBrightness){
      fire.style.filter = "brightness(" + brightnessWaxing + "%)";
      brightnessWaxing += 5;
  	  }
      if (filter < fireBrightness){
      brightnessWaning -= 1;
      fire.style.filter = "brightness(" + brightnessWaning + "%)"; 
      }
      if (brightnessWaxing >= 50){
  	  fire.style.zIndex = currentIndex;
  	  }
    }
  }
}

function removeFire(fire){   // destroy fire 
	var parent = document.getElementById("container");
	parent.removeChild(fire);
	fireBrightness = 0;
}

run();