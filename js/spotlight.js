var lightSize = window.innerHeight;
var spotSize = window.innerWidth / 10;

var colors = [{colorName: "aqua", colorHex: "#00ffff"}, 
              {colorName: "blue", colorHex: "#0000ff"}, 
              {colorName: "green", colorHex: "#00ff00"}, 
              {colorName: "lime", colorHex: "#bfff00"},
              {colorName: "magenta", colorHex: "#ff00ff"},  
              {colorName: "orange", colorHex: "#ffa500"},
              {colorName: "red", colorHex: "#ff0000"},
              {colorName: "yellow", colorHex: "#ffff00"}];

window.addEventListener("click", clicked);

function clicked(e) {
  var container = document.getElementById("container");
  while (container.firstChild){ container.removeChild(container.firstChild);}
  var lightColor = colors[getColor()].colorHex;
  var spotColor = colors[getColor()].colorHex;
  var x = event.screenX;
  var y = event.screenY;
  var height = spotSize / 4 + y;
  var width = spotSize;
  var spotlight = document.createElement("div");
  spotlight.classList.add("spotlight");
  var light = document.createElement("div");
  light.classList.add("light");
  var spot = document.createElement("div");
  spot.classList.add("spot");
  var element = document.getElementById("container");
  element.appendChild(spotlight);
  spotlight.appendChild(light);
  spotlight.appendChild(spot);
  spotlight.style.top = 0 + "px";
  spotlight.style.left = x - spotSize / 2 + "px";
  spotlight.style.height = height + "px";
  spotlight.style.width = spotSize + "px";
  light.style.borderLeft = spotSize / 2 + "px solid transparent";
  light.style.borderRight = spotSize / 2 + "px solid transparent";
  light.style.borderBottom = y + "px solid " + lightColor;
  spot.style.top = y - spotSize / 4 + "px"
  spot.style.width = spotSize + "px";
  spot.style.height = spotSize / 2 + "px";
  spot.style.background = "#fff";
  spot.style.borderRadius = spotSize / 2 + "px / " + spotSize / 4 + "px";
}

function getColor(){
  var color = Math.floor(Math.random() * colors.length);
  return color;
}