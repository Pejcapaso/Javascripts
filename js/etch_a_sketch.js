	let color = "red";
	let dimensionX = 16;
	let dimensionY = 16;
	let cellSizeX = 32;
	let cellSizeY = 32;
	let con = document.querySelector('#container');
	let control = document.querySelector('#control');
	let wrap = document.createElement('div');
	wrap.setAttribute('id', 'wrap'); 
	con.insertBefore(wrap, control); 

	wrap.style.height = dimensionY * cellSizeY + 'px';
	wrap.style.width = dimensionX * cellSizeX + 'px';

	for (let x = 0; x < dimensionX; x++){
		let wrapRow = document.createElement('div');
		wrapRow.classList.add('wrap-row');
		wrapRow.style.display = 'block';
		wrapRow.style.height = cellSizeY + 'px';
		wrapRow.style.width = dimensionX * cellSizeX + 'px';
		wrap.appendChild(wrapRow);

			for(let y = 0; y < dimensionY - 1; y++){
				let cell = document.createElement('div');
				cell.classList.add('cell');
				cell.style.display = 'inline-block';
				cell.style.height = cellSizeY + 'px';
				cell.style.width = cellSizeX + 'px';
				cell.style.border = '1px solid black';
				wrapRow.appendChild(cell);
			}
	}

	let container = document.querySelectorAll(".cell");
	let btnOnClick = document.querySelector("#onclick");
	btnOnClick.addEventListener('click', onClick);
	let btnOnMove = document.querySelector("#onmove");
	btnOnMove.addEventListener('click', onMove);


	function clearGrid(){
		container = document.querySelectorAll(".cell");
  		for (let z = 0; z < dimensionX * dimensionY; z++){
			container[z].style.background = '#fff';
		} 
	}

	let moveMode = 'none';

	function onClick() {
		container = document.querySelectorAll(".cell");
		if (moveMode == 'click' || moveMode == 'none'){
			moveMode = 'click';
  		for (let z = 0; z < dimensionX * dimensionY; z++){ 
			container[z].addEventListener('click', setColor); 
		}
		}
		if (moveMode == 'move'){
			moveMode = 'click';
  		for (let z = 0; z < dimensionX * dimensionY; z++){
  			container[z].removeEventListener('mouseover', setColor); 
			container[z].addEventListener('click', setColor); 
		}
		}
	}

	function onMove() {
		container = document.querySelectorAll(".cell");
		if (moveMode == 'move' || moveMode == 'none'){
			moveMode = 'move';
  		for (let z = 0; z < dimensionX * dimensionY; z++){
			container[z].addEventListener('mouseover', setColor); 
		}
		}
		if (moveMode == 'click'){
			moveMode = 'move';
  		for (let z = 0; z < dimensionX * dimensionY; z++){
  			container[z].removeEventListener('mouseover', setColor); 
			container[z].addEventListener('click', setColor); 
		}
		}
	}

	function setColor(e){
		e.target.style.background = color;
	}

	function setColorUser() {
		let enteredColor = document.getElementById("color").value;
		document.getElementById("color").value = null;
  		color = enteredColor;
	}

	function setGridSize(){
		let enteredGridSize = parseInt(document.getElementById("grid-size").value);
		if (isNaN(enteredGridSize)){
			alert("Please, enter the correct number");
		}
		else {
		document.getElementById("grid-size").value = null;
  		changeGridSize(enteredGridSize);
  		}
	}

	function setCellSize(){
		let enteredCellSize = parseInt(document.getElementById("cell-size").value);
		if (isNaN(enteredCellSize)){
			alert("Please, enter the correct number");
		}
		else {
		document.getElementById("cell-size").value = null;
  		changeCellSize(enteredCellSize);
  		}
	}

	function randomColor(){
		let ranColor = '#';
		let colorArr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f'];
			
			for (let x = 0; x < 6; x++){
				let ran = Math.floor((Math.random() * 16));
				ranColor += colorArr[ran];
			}
		color = ranColor;
	}

	function randomGridSize(){
			let ran = Math.floor((Math.random() * 100) + 2);
			changeGridSize(ran);
	}

	function randomCellSize(){
			let ran = Math.floor((Math.random() * 50) + 15);
			changeCellSize(ran);
	}

	function changeGridSize(gridSize){
		let element = document.getElementById("wrap");
		element.parentNode.removeChild(element);

		dimensionX = gridSize;
		dimensionY = gridSize;
		con = document.querySelector('#container');
		control = document.querySelector('#control');
		wrap = document.createElement('div');
		wrap.setAttribute('id', 'wrap'); 
		con.insertBefore(wrap, control); 

		wrap.style.height = dimensionY * cellSizeY + 'px';
		wrap.style.width = dimensionX * cellSizeX + 'px';


	 for (let x = 0; x < dimensionX; x++){
		let wrapRow = document.createElement('div');
		wrapRow.classList.add('wrap-row');
		wrapRow.style.display = 'block';
		wrapRow.style.height = cellSizeY + 'px';
		wrapRow.style.width = dimensionX * cellSizeX + 'px';
		wrap.appendChild(wrapRow);

			for(let y = 0; y < dimensionY - 1; y++){
				let cell = document.createElement('div');
				cell.classList.add('cell');
				cell.style.display = 'inline-block';
				cell.style.height = cellSizeY + 'px';
				cell.style.width = cellSizeX + 'px';
				cell.style.border = '1px solid black';
				wrapRow.appendChild(cell);
			}
	}
	}

	function changeCellSize(cellSize){

	let element = document.getElementById("wrap");
	element.parentNode.removeChild(element);

	let cellSizeX = cellSize;
	let cellSizeY = cellSize;
	let con = document.querySelector('#container');
	let control = document.querySelector('#control');
	let wrap = document.createElement('div');
	wrap.setAttribute('id', 'wrap'); 
	con.insertBefore(wrap, control); 

	wrap.style.height = dimensionY * cellSizeY + 'px';
	wrap.style.width = dimensionX * cellSizeX + 'px';
	console.log(wrap.style.height);
	console.log(wrap.style.width);

	for (let x = 0; x < dimensionX; x++){
		let wrapRow = document.createElement('div');
		wrapRow.classList.add('wrap-row');
		wrapRow.style.display = 'block';
		wrapRow.style.height = cellSizeY + 'px';
		wrapRow.style.width = dimensionX * cellSizeX + 'px';
		wrap.appendChild(wrapRow);

			for(let y = 0; y < dimensionY - 1; y++){
				let cell = document.createElement('div');
				cell.classList.add('cell');
				cell.style.display = 'inline-block';
				cell.style.height = cellSizeY + 'px';
				cell.style.width = cellSizeX + 'px';
				cell.style.border = '1px solid black';
				wrapRow.appendChild(cell);
			}
	}
	}
