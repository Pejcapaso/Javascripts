var lon = 0;
var lat = 0;
var city = "";
var arr = [];
var url;
var temp = "";
var tempUnits = "celcius";

//request for coordinates

if ("geolocation" in navigator) {
  navigator.geolocation.getCurrentPosition(function(location) {
  lat = location.coords.latitude;
  lon = location.coords.longitude;
});
}

var GetCoordsAjax = function(WhenDoneWithCoords){
  
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && xhr.status == 200) {
      var info = JSON.parse(xhr.responseText);
      console.log(info)
      if (lat == 0 && lon == 0){
      arr[0] = info.lat;
      arr[1] = info.lon;
      }
      var city = info.city;
      WhenDoneWithCoords(arr, city);
      //call for weather when coordinates are ready
      GetWeatherInfo(GetWeather);
} 
};
 xhr.open("GET", "http://ip-api.com/json", true);
 xhr.send();
}

function GetCoords(arr, GetCity){
  lat = arr[0];
  lon = arr[1];
  city = GetCity;
  document.getElementById("city-name").innerHTML += city + "<br>";
}

GetCoordsAjax(GetCoords);
//request for weather info
var GetWeatherInfo = function(WhenDoneWithWeather){
  url = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&APPID=47246b58dee8b9eba96fa2308e7d54e2";   
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && xhr.status == 200) {
      var info = JSON.parse(xhr.responseText);
      var weather = info.weather[0].main;
      console.log(weather);
      var cond = info.weather[0].id;
      var tempKelvin = info.main.temp;
      var tempCelsius = tempKelvin - 273.15;
      tempUnits = "celsius";
      var icon = "http://openweathermap.org/img/w/" + info.weather[0].icon;
      WhenDoneWithWeather(weather, cond, tempCelsius, icon); 
}  
};
 xhr.open("GET", url, true);
 xhr.send();
}

function GetWeather(WeatherInfo, condInfo, tempCelsiusInfo, iconInfo){
  var weather = WeatherInfo;
  var cond = condInfo;
  temp = tempCelsiusInfo;
  var icon = iconInfo;
  document.getElementById("icon").setAttribute("alt", weather);
  document.getElementById("icon").innerHTML =  '<img src = "' + icon + '.png"></img>' + '<br>';
  document.getElementById("temp").innerHTML = temp + "C";
  changeBack(cond);
}
function changeTemp() {
   if (tempUnits == "kelvin"){
     document.getElementById("temp").innerHTML = Math.round(temp) + "C";
     tempUnits = "celsius";
     return;
   }
   if (tempUnits == "celsius"){
     document.getElementById("temp").innerHTML = Math.round(temp * 9 / 5 + 32) + "F"; 
     tempUnits = "fahrenheit";
     return;
   }
   if (tempUnits == "fahrenheit"){
     document.getElementById("temp").innerHTML = Math.round(temp + 273.15) +  "K"; 
     tempUnits = "kelvin";
     return;
   }
}
function changeBack(cond){
    if (cond >= 200 && cond < 300){
      document.getElementById("background").style.backgroundImage = 'url("images/thunderstorm.gif")';
      document.getElementById("weather-sounds").src="sounds/thunderstorm.mp3";
    }
    if (cond >= 300 && cond < 400){
      document.getElementById("background").style.backgroundImage = 'url("images/drizzle.gif")';
      document.getElementById("weather-sounds").src="sounds/drizzle.mp3";
    }
    if (cond >= 500 && cond < 600){
      document.getElementById("background").style.backgroundImage = 'url("images/rain.gif")';
      document.getElementById("weather-sounds").src="sounds/rain.mp3";
    }
    if (cond >= 600 && cond < 700){
      document.getElementById("background").style.backgroundImage = 'url("images/snow.gif")';
      document.getElementById("weather-sounds").src="sounds/snow.mp3";
    }
    if (cond >= 700 && cond < 800){
      document.getElementById("background").style.backgroundImage = 'url("images/atmosphere.gif")';
      document.getElementById("weather-sounds").src="sounds/atmosphere.mp3";
    }
    if (cond == 800){
      document.getElementById("background").style.backgroundImage = 'url("images/clear.gif")'; 
      document.getElementById("weather-sounds").src="sounds/clear.mp3";
    }
    if (cond > 800 && cond < 900){
     document.getElementById("background").style.backgroundImage = 'url("images/clouds.gif")';
      document.getElementById("weather-sounds").src="sounds/clouds.mp3";
    }
}

var aud = document.getElementById("weather-sounds"); 

function playAudio() { 
    aud.play(); 
} 

function pauseAudio() { 
    aud.pause(); 
}