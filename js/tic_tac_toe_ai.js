var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var full = 0;
var countHuman = 0;
var countComp = 0;
var finished = 0;
(function(){  // draw a grid first time
ctx.beginPath();  
ctx.moveTo(100, 0);
ctx.lineTo(100, 300);
ctx.moveTo(200, 300);
ctx.lineTo(200, 0);
ctx.moveTo(300, 100);
ctx.lineTo(0, 100);
ctx.moveTo(0, 200);
ctx.lineTo(300, 200);
ctx.moveTo(300, 300);
ctx.lineTo(0, 300);
ctx.stroke();
}());
var grid = function(){  // draw a grid
ctx.beginPath();  
ctx.moveTo(100, 0);
ctx.lineTo(100, 300);
ctx.moveTo(200, 300);
ctx.lineTo(200, 0);
ctx.moveTo(300, 100);
ctx.lineTo(0, 100);
ctx.moveTo(0, 200);
ctx.lineTo(300, 200);
ctx.moveTo(300, 300);
ctx.lineTo(0, 300);
ctx.stroke();
};
var hiddenButtons = function(){    // when finished
    (function () {
  var visib = document.getElementsByClassName('button');
    for(i = 0; i < visib.length; i++) {
      visib[i].style.visibility = 'visible';
      }
  }());
  document.getElementById("play-again").onclick = playAgain;
  document.getElementById("quit-game").onclick = quitGame;
}
function playAgain(){   // continue playing
  finished = 0;
  full = 0;
  
  ctx.clearRect(0, 0, 300, 500);
  
  grid();
  
  for (i in corr){
         corr[i][4] = 0;
         corr[i][5] = 0;
         corr[i][6] = 0;
    }
  
  ctx.font = "30px Times New Roman";
    ctx.fillText(countHuman + ":" + countComp, 130, 400);
    
    (function () {
  var visib = document.getElementsByClassName('button');
    for(i = 0; i < visib.length; i++) {
      visib[i].style.visibility = 'hidden';
      }
  }());
}
function quitGame(){   // quit the game
  location.reload();
}
function check(){    // check if there's a winner
    if (corr.cell1[4] == 1 && corr.cell4[4] == 1 && corr.cell7[4] == 1){
      console.log("here");
    }
     for (var i = 4; i <= 5; i++){
     if ((corr.cell1[i] == 1 && corr.cell2[i] == 1 && corr.cell3[i] == 1) || (corr.cell1[i] == 1 && corr.cell4[i] == 1 && corr.cell7[i] == 1) || (corr.cell1[i] == 1 && corr.cell5[i] == 1 && corr.cell9[i] == 1) || (corr.cell2[i] == 1 && corr.cell5[i] == 1 && corr.cell8[i] == 1) || (corr.cell3[i] == 1 && corr.cell5[i] == 1 && corr.cell7[i] == 1) || (corr.cell1[i] == 1 && corr.cell5[i] == 1 && corr.cell9[i] == 1) || (corr.cell4[i] == 1 && corr.cell5[i] == 1 && corr.cell6[i] == 1) || (corr.cell7[i] == 1 && corr.cell8[i] == 1 && corr.cell9[i] == 1)){
          finished = 1;
               if (i == 4){
                for (i in corr){
                  console.log(corr[i][4]);
                }
                ctx.font = "30px Times New Roman";
          ctx.fillText("Human wins", 70, 350);
                countHuman++;
                hiddenButtons();
      }
        else if (i == 5){
        ctx.font = "30px Times New Roman";
          ctx.fillText("AI wins", 103, 350);
          countComp++;
          hiddenButtons();
      }
    }
  }
     full = 0;
     for (i in corr){
        full += corr[i][6];
      }
     if (full == 9) {
       ctx.font = "30px Times New Roman";
       ctx.fillText("Draw", 115, 350);
       finished = 1;
       hiddenButtons();
    }
}
          
var yourTurn = document.getElementById("canvas").onclick = getCoor; // get coordinates of your mouse
var corr = {   // coordinates of each cell
     cell1: [0, 100, 0, 100, 0, 0, 0],
     cell2: [100, 200, 0, 100, 0, 0, 0],
     cell3: [200, 300, 0, 100, 0, 0, 0],
     cell4: [0, 100, 100, 200, 0, 0, 0],
     cell5: [100, 200, 100, 200, 0, 0, 0],
     cell6: [200, 300, 100, 200, 0, 0, 0],
     cell7: [0, 100, 200, 300, 0, 0, 0],
     cell8: [100, 200, 200, 300, 0, 0, 0],
     cell9: [200, 300, 200, 300, 0, 0, 0]
};
function getCoor(){      // make your turn
  if (finished == 1){
    return 0;
  }
for (var i in corr){
     for (var j in corr){
  if (event.clientX > corr[i][0] && event.clientX < corr[i][1] && event.clientY > corr[i][2] && event.clientY <  corr[i][3] && corr[i][4] === 0 && corr[i][5] === 0){
     corr[i][4] = 1;
     corr[i][6] = 1;
     drawCross(i);
           check();
     takeTurns();
   }
    }
   }
}
function drawCross(i){ // draw a cross
    var sizer = 20;
    ctx.beginPath();
    ctx.moveTo(corr[i][0] + sizer, corr[i][2] + sizer);
    ctx.lineTo(corr[i][1] - sizer, corr[i][3] - sizer); 
    ctx.moveTo(corr[i][1] - sizer, corr[i][2] + sizer);
    ctx.lineTo(corr[i][0] + sizer, corr[i][3] - sizer);
    ctx.stroke();
}
function DrawNough(i){   //draw a nough
          var sizer = 20;
          ctx.beginPath();
          ctx.arc(corr[i][0]+50, corr[i][2]+50, 50 - sizer, 0, 2*Math.PI);
          ctx.stroke();
}
function takeTurns(){    // ai movement
      
  if (finished == 1){
    return 0;
  }
     //  protection
     if (((corr.cell1[4] == 1 && corr.cell2[4] == 1) || (corr.cell1[5] == 1 && corr.cell2[5] == 1))  && corr.cell3[4] == 0 && corr.cell3[5] == 0){
          DrawNough("cell3");
          corr.cell3[5] = 1;
          corr.cell3[6] = 1;
          check(); 
          return 0;
     }
      if (((corr.cell1[4] == 1 && corr.cell3[4] == 1) || (corr.cell1[5] == 1 && corr.cell3[5] == 1)) && corr.cell2[4] == 0 && corr.cell2[5] == 0){
          DrawNough("cell2");
          corr.cell2[5] = 1;
          corr.cell2[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell2[4] == 1 && corr.cell3[4] == 1) || (corr.cell2[5] == 1 && corr.cell3[5] == 1)) && corr.cell1[4] == 0 && corr.cell1[5] == 0){
          DrawNough("cell1");
          corr.cell1[5] = 1;
          corr.cell1[6] = 1;
             check(); 
          return 0;
     }
     if (((corr.cell1[4] == 1 && corr.cell4[4] == 1) || (corr.cell1[5] == 1 && corr.cell4[5] == 1)) && corr.cell7[4] == 0 && corr.cell7[5] == 0){
          DrawNough("cell7");
          corr.cell7[5] = 1;
          corr.cell7[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell1[4] == 1 && corr.cell7[4] == 1) || (corr.cell1[5] == 1 && corr.cell7[5] == 1)) && corr.cell4[4] == 0 && corr.cell4[5] == 0){
          DrawNough("cell4");
          corr.cell4[5] = 1;
          corr.cell4[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell4[4] == 1 && corr.cell7[4] == 1) || (corr.cell4[5] == 1 && corr.cell7[5] == 1)) && corr.cell1[4] == 0 && corr.cell1[5] == 0){
          DrawNough("cell1");
          corr.cell1[5] = 1;
          corr.cell1[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell1[4] == 1 && corr.cell5[4] == 1) || (corr.cell1[5] == 1 && corr.cell5[5] == 1)) && corr.cell9[4] == 0 && corr.cell9[5] == 0){
          DrawNough("cell9");
          corr.cell9[5] = 1;
          corr.cell9[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell1[4] == 1 && corr.cell9[4] == 1) || (corr.cell1[5] == 1 && corr.cell9[5] == 1)) && corr.cell5[4] == 0 && corr.cell5[5] == 0){
          DrawNough("cell5");
          corr.cell5[5] = 1;
          corr.cell5[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell5[4] == 1 && corr.cell9[4] == 1) || (corr.cell5[5] == 1 && corr.cell9[5] == 1)) && corr.cell1[4] == 0 && corr.cell1[5] == 0){
          DrawNough("cell1");
          corr.cell1[5] = 1;
          corr.cell1[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell2[4] == 1 && corr.cell5[4] == 1) || (corr.cell2[5] == 1 && corr.cell5[5] == 1)) && corr.cell8[4] == 0 && corr.cell8[5] == 0){
          DrawNough("cell8");
          corr.cell8[5] = 1;
          corr.cell8[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell2[4] == 1 && corr.cell8[4] == 1) || (corr.cell2[5] == 1 && corr.cell8[5] == 1)) && corr.cell5[4] == 0 && corr.cell5[5] == 0){
          DrawNough("cell5");
          corr.cell5[5] = 1;
          corr.cell5[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell5[4] == 1 && corr.cell8[4] == 1) || (corr.cell5[5] == 1 && corr.cell8[5] == 1)) && corr.cell2[4] == 0 && corr.cell2[5] == 0){
          DrawNough("cell2");
          corr.cell2[5] = 1;
          corr.cell2[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell3[4] == 1 && corr.cell5[4] == 1) || (corr.cell3[5] == 1 && corr.cell5[5] == 1))&& corr.cell7[4] == 0 && corr.cell7[5] == 0){
          DrawNough("cell7");
          corr.cell7[5] = 1;
          corr.cell7[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell3[4] == 1 && corr.cell7[4] == 1) || (corr.cell3[5] == 1 && corr.cell7[5] == 1)) && corr.cell5[4] == 0 && corr.cell5[5] == 0){
          DrawNough("cell5");
          corr.cell5[5] = 1;
          corr.cell5[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell5[4] == 1 && corr.cell7[4] == 1) || (corr.cell5[5] == 1 && corr.cell7[5] == 1)) && corr.cell3[4] == 0  && corr.cell3[5] == 0){
          DrawNough("cell3");
          corr.cell3[5] = 1;
          corr.cell3[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell3[4] == 1 && corr.cell6[4] == 1) || (corr.cell3[5] == 1 && corr.cell6[5] == 1)) && corr.cell9[4] == 0 && corr.cell9[5] == 0){
          DrawNough("cell9");
          corr.cell9[5] = 1;
          corr.cell9[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell3[4] == 1 && corr.cell9[4] == 1) || (corr.cell3[5] == 1 && corr.cell9[5] == 1)) && corr.cell6[4] == 0 && corr.cell6[5] == 0){
          DrawNough("cell6");
          corr.cell6[5] = 1;
          corr.cell6[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell6[4] == 1 && corr.cell9[4] == 1) || (corr.cell6[5] == 1 && corr.cell9[5] == 1)) && corr.cell3[4] == 0 && corr.cell3[5] == 0){
          DrawNough("cell3");
          corr.cell3[5] = 1;
          corr.cell3[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell4[4] == 1 && corr.cell5[4] == 1) || (corr.cell4[5] == 1 && corr.cell5[5] == 1)) && corr.cell6[4] == 0 && corr.cell6[5] == 0){
          DrawNough("cell6");
          corr.cell6[5] = 1;
          corr.cell6[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell4[4] == 1 && corr.cell6[4] == 1) || (corr.cell4[5] == 1 && corr.cell6[5] == 1)) && corr.cell5[4] == 0 && corr.cell5[5] == 0){
          DrawNough("cell5");
          corr.cell5[5] = 1;
          corr.cell5[6] = 1;
             check(); 
          return 0;
     }
      if (((corr.cell5[4] == 1 && corr.cell6[4] == 1) || (corr.cell5[5] == 1 && corr.cell6[5] == 1)) && corr.cell4[4] == 0 && corr.cell4[5] == 0){
          DrawNough("cell4");
          corr.cell4[5] = 1;
          corr.cell4[6] = 1;
               check(); 
          return 0;
     }
     if (((corr.cell7[4] == 1 && corr.cell8[4] == 1) || (corr.cell7[5] == 1 && corr.cell8[5] == 1)) && corr.cell9[4] == 0 && corr.cell9[5] == 0){
          DrawNough("cell9");
          corr.cell9[5] = 1;
          corr.cell9[6] = 1;
          check(); 
          return 0;
     }
      if (((corr.cell7[4] == 1 && corr.cell9[4] == 1) || (corr.cell7[5] == 1 && corr.cell9[5] == 1)) && corr.cell8[4] == 0 && corr.cell8[5] == 0){
          DrawNough("cell8");
          corr.cell8[5] = 1;
          corr.cell8[6] = 1;
          check(); 
          return 0;
     }
      if (((corr.cell8[4] == 1 && corr.cell9[4] == 1) || (corr.cell8[5] == 1 && corr.cell9[5] == 1)) && corr.cell7[4] == 0 && corr.cell7[5] == 0){
          DrawNough("cell7");
          corr.cell7[5] = 1;
          corr.cell7[6] = 1;
          check(); 
          return 0;
     }
        // prophylaxis
           if (corr.cell5[4] == 0 && corr.cell5[5] == 0){
            DrawNough("cell5");
          corr.cell5[5] = 1;
          corr.cell5[6] = 1;
          check(); 
          return 0;   
        }
           if ((corr.cell5[4] == 1 || corr.cell5[5] == 1) && (corr.cell1[4] == 0 && corr.cell1[5] == 0) ){
            DrawNough("cell1");
          corr.cell1[5] = 1;
          corr.cell1[6] = 1;
          check(); 
          return 0;
      }
     /*    if ((corr.cell5[4] == 1 || corr.cell5[5] == 1) && (corr.cell9[4] == 1 || corr.cell9[5] == 1)  && (corr.cell3[4] == 0 && corr.cell3[5] == 0)){
            DrawNough("cell3");
          corr.cell3[5] = 1;
          corr.cell3[6] = 1;
          check(); 
          return 0;
      }*/
        if ((corr.cell1[4] == 1) && (corr.cell6[4] == 1)  && (corr.cell3[4] == 0 && corr.cell3[5] == 0)){
            DrawNough("cell3");
          corr.cell3[5] = 1;
          corr.cell3[6] = 1;
          check(); 
          return 0;
      }
         if ((corr.cell1[4] == 1) && (corr.cell8[4] == 1)  && (corr.cell7[4] == 0 && corr.cell7[5] == 0)){
            DrawNough("cell7");
          corr.cell7[5] = 1;
          corr.cell7[6] = 1;
          check(); 
          return 0;
      }
        if ((corr.cell2[4] == 1) && (corr.cell4[4] == 1)  && (corr.cell1[4] == 0 && corr.cell1[5] == 0)){
            DrawNough("cell1");
          corr.cell1[5] = 1;
          corr.cell1[6] = 1;
          check(); 
          return 0;
      }
        if ((corr.cell2[4] == 1) && (corr.cell6[4] == 1)  && (corr.cell3[4] == 0 && corr.cell3[5] == 0)){
            DrawNough("cell3");
          corr.cell3[5] = 1;
          corr.cell3[6] = 1;
          check(); 
          return 0;
      }   
        if ((corr.cell2[4] == 1) && (corr.cell7[4] == 1)  && (corr.cell1[4] == 0 && corr.cell1[5] == 0)){
            DrawNough("cell1");
          corr.cell1[5] = 1;
          corr.cell1[6] = 1;
          check(); 
          return 0;
      }
         if ((corr.cell2[4] == 1) && (corr.cell9[4] == 1)  && (corr.cell3[4] == 0 && corr.cell3[5] == 0)){
            DrawNough("cell3");
          corr.cell3[5] = 1;
          corr.cell3[6] = 1;
          check(); 
          return 0;
      }
        if ((corr.cell3[4] == 1) && (corr.cell4[4] == 1)  && (corr.cell1[4] == 0 && corr.cell1[5] == 0)){
            DrawNough("cell1");
          corr.cell1[5] = 1;
          corr.cell1[6] = 1;
          check(); 
          return 0;
      }
        if ((corr.cell4[4] == 1) && (corr.cell8[4] == 1)  && (corr.cell7[4] == 0 && corr.cell7[5] == 0)){
            DrawNough("cell7");
          corr.cell7[5] = 1;
          corr.cell7[6] = 1;
          check(); 
          return 0;
      }
        if ((corr.cell4[4] == 1) && (corr.cell9[4] == 1)  && (corr.cell7[4] == 0 && corr.cell7[5] == 0)){
            DrawNough("cell7");
          corr.cell7[5] = 1;
          corr.cell7[6] = 1;
          check(); 
          return 0;
      }
        if ((corr.cell6[4] == 1) && (corr.cell7[4] == 1)  && (corr.cell9[4] == 0 && corr.cell9[5] == 0)){
            DrawNough("cell9");
          corr.cell9[5] = 1;
          corr.cell9[6] = 1;
          check(); 
          return 0;
      }
         if ((corr.cell6[4] == 1) && (corr.cell8[4] == 1)  && (corr.cell9[4] == 0 && corr.cell9[5] == 0)){
            DrawNough("cell9");
          corr.cell9[5] = 1;
          corr.cell9[6] = 1;
          check(); 
          return 0;
      }
       else  {
        for (i in corr){
          if (corr[i][4] == 0 && corr[i][5] == 0){
            DrawNough(i);
          corr[i][5] = 1;
          corr[i][6] = 1;
          check(); 
          return 0; 
          }
        }
    }
}