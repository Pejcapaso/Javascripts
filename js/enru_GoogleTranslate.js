class enru_GoogleTranslate {
		    constructor(options) {
		        this.options = options;
		        this.maxexample = 5;
		        this.word = '';
		    }

		    async displayName() {
		        let locale = await api.locale();
		        if (locale.indexOf('CN') != -1) return 'enru_GoogleTranslate';
		        if (locale.indexOf('TW') != -1) return 'enru_GoogleTranslate';
		        return 'enru_GoogleTranslate';
		    }

		    setOptions(options) {
		        this.options = options;
		        this.maxexample = options.maxexample;
		    }

		    async findTerm(word) {
		        this.word = word;
		        return await this.findMultitran(word);
		    }

		    async findMultitran(word) {
		        if (!word) return null;
			let base = 'https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=ru&dt=t&q='
			let url =  base + encodeURIComponent(word);		
			try {    
		  	fetch(url)
			.then(response => {
			       if (!response.ok) {
			           throw new Error("HTTP error " + response.status);
			       }
			       return response.json();
			   })
			   .then(json => {
   		        let content = json[0][0][0];
   		        if (!content) return null;
   		        let css = this.renderCSS();
   		        return css + content;
			   })
			   .catch(function () {
				   console.log('error');
			       return null;
			   })
			   
		   } catch (err) {
		            return null;
		        }

		    }

		    renderCSS() {
		        let css = `
		            <style>
		                .copyright{
		                    display:none;
		                }
		                .orth {
		                    font-size: 100%;
		                    font-weight: bold;
		                }
		                .quote {
		                    font-style: normal;
		                    color: #1683be;
		                }
		                .colloc {
		                    font-style: italic;
		                    font-weight: normal;
		                }
		                .sense {
		                    border: 1px solid;
		                    border-color: #e5e6e9 #dfe0e4 #d0d1d5;
		                    border-radius: 3px;
		                    padding: 5px;
		                    margin-top: 3px;
		                }
		                .sense .re {
		                    font-size: 100%;
		                    margin-left: 0;
		                }
		                .sense .sense {
		                    border: initial;
		                    border-color: initial;
		                    border-radius: initial;
		                    padding: initial;
		                    margin-top: initial;
		                }
		                a {
		                    color: #000;
		                    text-decoration: none;
		                }
		                * {
		                    word-wrap: break-word;
		                    box-sizing: border-box;
		                }
		            </style>`;

		        return css;
		    }
		}

		let word = new enru_GoogleTranslate();
		word.findTerm('list');