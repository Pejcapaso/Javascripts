function getColor() {
  return (
    "#" + Math.random()
      .toString(16)
      .slice(2, 8)
  );
}

function setBackground() {
  var bgColor = getColor();
  document.body.style.background = bgColor;
  document.getElementById("info").innerHTML = bgColor;
}

function setTextColor(){
	var textColor = document.getElementById("info").innerHTML;
	var newTextColor = invertColor(textColor, 1);
	document.getElementById("hex").style.color = newTextColor;
	document.getElementById("info").style.color  = newTextColor;
}

function invertColor(hex, bw) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }

    if (hex.length !== 6) {
        throw new Error('Invalid HEX color.');
    }
    var r = parseInt(hex.slice(0, 2), 16),
        g = parseInt(hex.slice(2, 4), 16),
        b = parseInt(hex.slice(4, 6), 16);
    if (bw) {
 
        return (r * 0.299 + g * 0.587 + b * 0.114) > 186
            ? '#000000'
            : '#FFFFFF';
    }
    // invert color components
    r = (255 - r).toString(16);
    g = (255 - g).toString(16);
    b = (255 - b).toString(16);
    // pad each with zeros and return
    return "#" + padZero(r) + padZero(g) + padZero(b);
}

setBackground();
setTextColor();

document.body.onkeyup = function(e) {
  if (e.keyCode == 32) {
    setBackground();
    setTextColor();
  }
};