let stars = [];
let spaceWidth = window.innerWidth;
let spaceHeight = window.innerHeight;
window.requestAnimationFrame(createStar); // initialize the animation
var starSize = window.innerHeight;
var colors = [{colorName: "aqua", colorHex: "#00ffff"}, 
              {colorName: "blue", colorHex: "#0000ff"}, 
              {colorName: "green", colorHex: "#00ff00"}, 
              {colorName: "lime", colorHex: "#bfff00"},
              {colorName: "magenta", colorHex: "#ff00ff"},  
              {colorName: "orange", colorHex: "#ffa500"},
              {colorName: "red", colorHex: "#ff0000"},
              {colorName: "yellow", colorHex: "#ffff00"},
              {colorName: "white", colorHex: "#ffffff"}];
var starColor = getColor();

function createStar(){  // making a star
if (stars.length < 300 && Math.random() < .5){ // if fewer than 300 stars, a 50% chance of creating a new one	             
          let star = {x: 0, y: 0, vx: -5 + Math.random() * 10, vy: -5 + Math.random() * 10} // create a new star in the middle with random velocity	         
          stars.push(star);               
}
 let container = document.getElementById("container");
  while (container.firstChild){ container.removeChild(container.firstChild);}
  for(let n = 0; n < stars.length; n++){ 	            
       stars[n].x = stars[n].x + stars[n].vx; 	                
       stars[n].y = stars[n].y + stars[n].vy;	                
       if(stars[n].x > window.innerWidth || stars[n].x < -window.innerWidth){ // if the star is out of bounds	                       
       stars[n].x = 0;  // put it back in the center	                        
       stars[n].y = 0;	                
       }	       
       let stardiv = document.createElement("div");
       stardiv.classList.add("star");
       stardiv.style.filter = "brightness(" + Math.floor((Math.abs(stars[n].x) + Math.abs(stars[n].y)) / 2) + "%)";    
       stardiv.style.backgroundColor = colors[starColor].colorHex;
       stardiv.style.top = window.innerHeight / 2 + stars[n].x + "px";
       stardiv.style.left = window.innerWidth / 2 + stars[n].y + "px";
       stardiv.style.height = Math.abs(stars[n].y / 50 + n / 200) + "px";
       stardiv.style.width = Math.abs(stars[n].y / 50 + n / 200) + "px";
       let element = document.getElementById("container");
	   element.appendChild(stardiv);	        
       }	        
       window.requestAnimationFrame(createStar);
}

function getColor(){  // picking the random color
var color = Math.floor(Math.random() * colors.length);
return color;
}