var eyeHeight = 50;
var eyeWidth = 110;
var colors = [{colorName: "blue", colorHex: "#0000ff", colorInvert: "#fff"}, 
              {colorName: "cyan", colorHex: "#00ffff", colorInvert: "#000"},
              {colorName: "green", colorHex: "#00ff00", colorInvert: "#000"}, 
              {colorName: "magenta", colorHex: "#ff00ff", colorInvert: "#fff"},  
              {colorName: "red", colorHex: "#ff0000", colorInvert: "#fff"},
              {colorName: "yellow", colorHex: "#ffff00", colorInvert: "#000"}];

function createEyes(){
    var eyes = document.createElement("div");
    eyes.classList.add("eyes");
    var el = document.getElementById("container");
    el.appendChild(eyes);
    var leftEye = document.createElement("div");
    leftEye.classList.add("left-eye")
    eyes.appendChild(leftEye);
    var rightEye = document.createElement("div");
    rightEye.classList.add("right-eye")
    eyes.appendChild(rightEye);
    var leftPupil = document.createElement("div");
    leftPupil.classList.add("left-pupil")
    leftEye.appendChild(leftPupil);
    var rightPupil = document.createElement("div");
    rightPupil.classList.add("right-pupil")
    rightEye.appendChild(rightPupil);
    var coordinates = getCoordinates();
    eyes.style.top = coordinates[1] + "px";
    eyes.style.left = coordinates[0] + "px";
    var color = getColor();
    leftEye.style.backgroundColor = colors[color].colorHex;
    rightEye.style.backgroundColor = colors[color].colorHex;
    leftPupil.style.backgroundColor = colors[color].colorInvert;
    rightPupil.style.backgroundColor = colors[color].colorInvert;
    blink(eyes);
}

function destroyEyes(eyes){
    var parent = document.getElementById("container");
    parent.removeChild(eyes);
}

function getCoordinates(){  // random coordinates
    var coordX = Math.floor(Math.random() * (window.innerWidth - eyeWidth));   // appear completely inside the viewport
    var coordY = Math.floor(Math.random() * (window.innerHeight - eyeWidth));
    console.log(window.innerWidth, coordX, coordY, eyeWidth);
    return [coordX, coordY];
}

function randomTime(){
    var time = Math.floor(Math.random() * 5000 + 1000);
    return time;
}

function setTime(){
    var timer = randomTime();
    setTimeout(createEyes, timer);
}

function getColor(){  // picking random color
    var color = Math.floor(Math.random() * colors.length);
    return color;
}

function blink(eyes){  // blink logic
   var outerId = setInterval(blinkFrequency, 3000);
   var outerFilter = getBlinks();
   function getBlinks(){
        var blinks = Math.floor(Math.random() * 5) + 1;
        if (blinks % 2 == 0){
            blinks++;
        }
        return blinks;
    }
   function blinkFrequency(){
    if (outerFilter == 0){
    clearInterval(outerId);
    destroyEyes(eyes);
    setTime();
    }
   else{
   outerFilter--;
   var id = setInterval(blinkToggle, 300);
   var brightnessSwitch = 100;
   var brightness = 1;
   eyes.style.filter = "brightness(" + brightnessSwitch + "%)";
   var filter = 2;
   function blinkToggle() {
    if (filter == 0) {
      clearInterval(id);
    } else { 
      filter--;
      if (brightnessSwitch == 100){
      brightness = 1;
      }
      if (brightnessSwitch == 0){
      brightness = 0;
      }
      if (brightness == 1){
      brightnessSwitch -= 100;
      eyes.style.filter = "brightness(" + brightnessSwitch + "%)"; 
      }
      if (brightness == 0){
      brightnessSwitch += 100;
      eyes.style.filter = "brightness(" + brightnessSwitch + "%)"; 
      }
     }
    }
   }
  }
 }

 createEyes();