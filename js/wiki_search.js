function gotoRandom(){
  window.location.href = "https://en.wikipedia.org/wiki/Special:Random";
}
var searched;
 
  var getSearchedArticles = function(getResults){
  var url = 'https://cors.io?https://en.wikipedia.org/w/api.php?action=opensearch&format=json&search=' + searched;  // cors.io to proxy request
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && xhr.status == 200) {
      var resp = JSON.parse(xhr.responseText);
      var title = resp[1];
      var descr = resp[2];
      var link = resp[3];
 getResults(title, descr, link);
}
};
 xhr.open("GET", url, true);
 xhr.send();
}
function setSearchedArticles(t, d, l){
      var x = document.getElementById("new-searched-results");
      x.parentNode.removeChild(x);
      var div = document.createElement("div");
      div.setAttribute("id", 'new-searched-results');
      document.getElementById("article-search-results").appendChild(div);
  for (var i = 0; i < t.length; i++){
    var link = document.createElement("a");
    link.setAttribute("href", l[i]);
    var breakLine = document.createElement("br");
    var linkName = document.createTextNode(t[i]); 
    var linkDescr = document.createTextNode(d[i]);
    link.appendChild(linkName);
    link.appendChild(breakLine);
    link.appendChild(linkDescr);                         
    document.getElementById("new-searched-results").appendChild(link);
  }
}
function searchTrigger(){  
    searched = document.getElementById("search-field").value;
    getSearchedArticles(setSearchedArticles);
}