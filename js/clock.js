var secondsHand = document.querySelector('.seconds-hand');
  	var minutesHand = document.querySelector(`.minutes-hand`);
  	var hoursHand = document.querySelector(`.hours-hand`);

  	function setDate(){

  		var time = new Date();
  		var seconds = time.getSeconds();
      var digitalSeconds = String(seconds).padStart(2, "0");
      document.getElementById("seconds").innerHTML = digitalSeconds;
  		var degreesSeconds = seconds * 6 + 90;
  		secondsHand.style.transform = `rotate(${degreesSeconds}deg)`;
  		var minutes = time.getMinutes();
      var digitalMinutes = String(minutes).padStart(2, "0");
      document.getElementById("minutes").innerHTML = digitalMinutes + ":";
  		var degreesMinutes = minutes * 6 + 90;
  		minutesHand.style.transform = `rotate(${degreesMinutes}deg)`;
  		var hours = time.getHours();
      var digitalHours = String(hours).padStart(2, "0");
      document.getElementById("hours").innerHTML = digitalHours + ":";
  		var degreesHours = hours * 30 + 90;
  		hoursHand.style.transform = `rotate(${degreesHours}deg)`;
  	}

  	setInterval(setDate, 1000);