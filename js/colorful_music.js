  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  var canvasWidth = canvas.width;
  var canvasHeight = canvas.height;
  var circleCenterX = canvasWidth / 2;
  var circleCenterY = canvasHeight / 2;
  var radius = canvasHeight / 50;
  var radiusEnlarger = 0.5;
  var radiusDots = canvasHeight / 50;
  window.addEventListener("click", clicked);
  var requestAnimationFrame = window.requestAnimationFrame || 
                            window.mozRequestAnimationFrame || 
                            window.webkitRequestAnimationFrame || 
                            window.msRequestAnimationFrame;
  var dotsStorage = {
  	x: [],
  	y: [],
  	color: [],
  	border: []
  }
  var dotColors = [
  	{colorName: "apricot", colorHex: "#fbceb1"},
  	{colorName: "aqua", colorHex: "#00ffff"}, 
  	{colorName: "beige", colorHex: "#f5f5dc"},
  	{colorName: "blue", colorHex: "#0000ff"},
  	{colorName: "brown", colorHex: "#a52a2a"}, 
  	{colorName: "coral", colorHex: "ff7f50"},
  	{colorName: "cyan", colorHex: "#00ffff"},
  	{colorName: "gray", colorHex: "#808080"}, 
  	{colorName: "green", colorHex: "#00ff00"}, 
  	{colorName: "indigo", colorHex: "#4b0082"},
  	{colorName: "lavender", colorHex: "#e6e6fa"}, 
  	{colorName: "lime", colorHex: "#bfff00"},
  	{colorName: "magenta", colorHex: "#ff00ff"}, 
  	{colorName: "maroon", colorHex: "#800000"}, 
  	{colorName: "mauve", colorHex: "#e0b0ff"}, 
  	{colorName: "mint", colorHex: "#3eb489"}, 
  	{colorName: "olive", colorHex: "#808000"},
  	{colorName: "orange", colorHex: "#ffa500"},
  	{colorName: "pink", colorHex: "#ffc0cb"},
  	{colorName: "purple", colorHex: "#800080"}, 
  	{colorName: "red", colorHex: "#ff0000"},
  	{colorName: "scarlet", colorHex: "#ff2400"},
  	{colorName: "teal", colorHex: "#008080"},
  	{colorName: "turquoise", colorHex: "#30d5c8"}, 
  	{colorName: "violet", colorHex: "#ee82ee"},
  	{colorName: "yellow", colorHex: "#ffff00"}
  ]

function draw() {
ctx.clearRect(0, 0, canvasWidth, canvasHeight);
drawCircle();  // drawing playing circle
drawDots();    // drawing dots
requestAnimationFrame(draw); // launch animation
check();  // check conditions
}

function drawCircle(){
ctx.beginPath();
radius += radiusEnlarger;
ctx.strokeStyle = "#fff";
ctx.lineWidth = 1;
ctx.arc(circleCenterX, circleCenterY, radius, 0, 2 * Math.PI);
ctx.fillStyle = "#000";
ctx.fill();
ctx.stroke();
} 

function drawDots(){
 for (var i = 0; i < dotsStorage.x.length; i++){
 ctx.beginPath();
 if (dotsStorage.border[i] == 1){   // if dot was touched
   ctx.strokeStyle = "#fff";
   ctx.lineWidth = 3;
 }
 else ctx.strokeStyle = "#000";
 ctx.arc(dotsStorage.x[i], dotsStorage.y[i], radiusDots, 0, 2 * Math.PI);
 ctx.fillStyle = dotsStorage.color[i];
 ctx.fill();
 ctx.stroke();
}
}

function drawDot(x, y){
	dotColor = getDotsColors();
	dotsStorage.color.push(dotColor);
	dotsStorage.x.push(x);
 	dotsStorage.y.push(y);
}

function clicked(event) {
 var x = event.clientX;
 var y = event.clientY;
 drawDot(x, y);
}

function getDotsColors(){  
var random = Math.floor((Math.random() * 26));
return dotColors[random].colorHex;
}

function check() {

	// will destroy the circles if the radius becomes too big for a screen
	if (radius > canvasHeight / 2 && radius > canvasWidth / 2){
	radius = 0;
	dotsStorage = {
  	x: [],
  	y: [],
  	color: [],
  	border: []
 	}
	}  

	// if dot was touched
	if (dotsStorage.x.length > 0){
		for (var i = 0; i < dotsStorage.x.length; i++){
		var distance = findTheDistance(dotsStorage.x[i], dotsStorage.y[i]);
		if (Math.floor(distance) == Math.floor(radius + radiusDots)){
			dotsStorage.border[i] = 1;
			for (var j = 0; j < dotColors.length; j++){
				if (dotColors[j].colorHex == dotsStorage.color[i]){
			document.getElementById(dotColors[j].colorName).play();
			}} 
		}
		}
	}   
}

function findTheDistance(x, y){
	var absX = x - circleCenterX;
	var absY = y - circleCenterY;
	if (absX < 0){
		absX *= -1;
	}
	if (absY < 0){
		absY *= -1; 
	}
	return Math.sqrt(absY * absY + absX * absX);
}

  draw();  // launch everything