# Jscripts

A bunch of js apps and scripts I wrote for fun: https://pejcapaso.codeberg.page/javascripts/

![Javascripts](images/javascripts.png)

* Calculator
* Digital and analog clock
* Colourful Music
* Pixel drawing
* Hex colors generator
* Illusion app
* Live code editor
* Blinking eyes
* Pomodoro
* Simon game
* Spotlight emulator
* Through the Universe emulator
* Tic Tac Toe
* Weather emulator
* Wikipedia searcher
* Ghost lights animation 